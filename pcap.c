#include "pcap.h"

int XDcfg_TransferBitfile(XDcfg *DcfgInstPtr, int PartialCfg, u32 PartialAddress, u32 bitfile_length_words)
{
     u32 IntrStsReg = 0;

     XDcfg_Transfer(DcfgInstPtr, (u8 *)PartialAddress, bitfile_length_words,
                     (u8 *)XDCFG_DMA_INVALID_ADDRESS,
                                     0, XDCFG_NON_SECURE_PCAP_WRITE);

     // Poll IXR_DMA_DONE
     IntrStsReg = XDcfg_IntrGetStatus(DcfgInstPtr);
     while ((IntrStsReg & XDCFG_IXR_DMA_DONE_MASK) !=
                                     XDCFG_IXR_DMA_DONE_MASK) {
                 IntrStsReg = XDcfg_IntrGetStatus(DcfgInstPtr);
                         }

     if (PartialCfg) {
          /* Poll IXR_D_P_DONE */
          while ((IntrStsReg & XDCFG_IXR_D_P_DONE_MASK) !=
                                                  XDCFG_IXR_D_P_DONE_MASK) {
                                      IntrStsReg = XDcfg_IntrGetStatus(DcfgInstPtr);
                                                      }
     } else {
          /* Poll IXR_PCFG_DONE */
          while ((IntrStsReg & XDCFG_IXR_PCFG_DONE_MASK) !=
                                                  XDCFG_IXR_PCFG_DONE_MASK) {
                                      IntrStsReg = XDcfg_IntrGetStatus(DcfgInstPtr);
                                                      }
              // Enable the level-shifters from PS to PL.
              Xil_Out32(SLCR_UNLOCK, SLCR_UNLOCK_VAL);
                              Xil_Out32(SLCR_LVL_SHFTR_EN, 0xF);
                                              Xil_Out32(SLCR_LOCK, SLCR_LOCK_VAL);
     }
     return XST_SUCCESS;
}

//#ifdef 0
int main()
{
    debug_printf("Hello world!\n");
}
//#endif
