#include <stdio.h>
#include <stdlib.h>
#include "config-api.h"

void main()
{
    FILE file="./zed_add.xml";
    int island = 0;
    int result;

    // check resource available
    report_available_regions();
    // place the accelerator on the chosen island
    result = place_accelerator_ptr(file, island);
    if (result < 0)
        printf("Failed to place module on the island %d\n", island);
    // check resource running
    report_running_regions();
}
