#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int	CLKFrames;
int	IOBFrames;
int	IOIFrames;
int	CLBFrames;
int	RAMFrames;
int	RI_Frames;				// number of BRAM interconnect frames
int MFrames;
int DFrames;
int GFrames;
int HWords;
int	CLBBytesIO;				// amount of Bytes for the CLB-IO-blocks within each frame
int	CLBBytes;
int	NFrames;
int FLength;			// frame length can be set by writing a device ID
int NColCLB;
int NColRAM;
int NULLArea;
int NULLArea1;
int NULLArea2;
int NULLArea3;
int RowFrames;
int RowFrames2;
int RAMRowOff;
int RAMRowFrames;
int NRowRsc;
int LutBitOffset[8][16];		// frame offset values for all LUTs Slice3,G downto Slice0,F and Bit F downto 0
int LutShiftUsageOffset[8][4];		// for detecting reconfigurable select generator usage; we check up to 4 bit; set to -1 if unused
char *ResourceString;
char *ResourceString2;
char *Device;	// device identifier
char *Family;	// FPGA device family

int BA;   				// block address
int TB;				// top/bottom bit
int RA;				// row address
int MJA;  				// major address
int MNA;  				// minor address

int NColNULL;
int NColRI;
int NColBFG;
int NColM;
int NColD;

struct DeviceParameters {
	int NFrames;
	int FLength;
	int NColCLB;
	int NColRAM;
	int NRowRsc;
	char *ResourceString;
	char *ResourceString2;
	char *Family;
	int CLKFrames;
	int	IOBFrames;
	int	IOIFrames;
	int	CLBFrames;	// CLBL frames
	int	RAMFrames;	// Block RAM frames
	int	RI_Frames;	// Block RAM interconnect frames
	int MFrames;	// CLBM frames
	int DFrames;	// DSP frames
	int GFrames;	// BUFG frames
	int HWords;	// RCLK frames
	int	CLBBytesIO;
	int	CLBBytes;
	int LutBitOffset[8][16];
	int LutShiftUsageOffset[8][4];
	int NULLArea;
	int NULLArea1;
	int NULLArea2;
	int NULLArea3;
	int RowFrames;
	int RowFrames2;
	int RAMRowOff;
	int RAMRowFrames;
};

//	struct DeviceParameters *GlobalParameters;

//const char TB_Max = 2;

int getGlobalDeviceInformation(int DeviceID);
int SetGlobalDeviceParameters(char *Device);
int SetDeviceParameters(char *Device, struct DeviceParameters *Parameter);
int getFrameIndexGlobal(void);
int getFrameIndex(int BA, int MJA, int MNA, int TB_RA);	// TB_RA = TB + RA
int incFrameAddress(void);
int getROWByteIndex(int CLB_ROWindex);
int echoSupportedDevices(void);
int FrameADR2BlockADR(int FrameADR, int *BA, int *MJA, int *MNA);

int getDeviceInformation(int DeviceID);
int SetDeviceParameters(char *Device, struct DeviceParameters *Parameter);
int SetGlobalDeviceParameters(char *Device);
int getFrameIndexGlobal(void);
int getFrameIndex(int BA, int MJA, int MNA, int TB_RA);
int incFrameAddress(void);
//int getROWByteIndex(int CLB_ROWindex)
//{
//	if(CLB_ROWindex==0)
//		return(0);
//	else
//		return(CLBBytesIO + (CLB_ROWindex-1)*CLBBytes);
//}
//int FrameADR2BlockADR(int FrameADR, int *BA, int *MJA, int *MNA)
//{
//  int tmp;
//	if(FrameADR<CLKFrames+2*IOBFrames+2*IOIFrames+NColCLB*CLBFrames)	// we are in the CLB section
//	{
//		*BA=0;
//		if(FrameADR<CLKFrames)											// GCLK column
//		{
//			*MJA=0;
//			*MNA=FrameADR;
//		}
//		else if(FrameADR-CLKFrames<IOBFrames)							// left IOB
//		{
//			*MJA=1;
//			*MNA=FrameADR-CLKFrames;
//		}
//		else if(FrameADR-CLKFrames-IOBFrames<IOIFrames)					// left IOI
//		{
//			*MJA=2;
//			*MNA=FrameADR-CLKFrames-IOBFrames;
//		}
//		else if(FrameADR-CLKFrames-IOBFrames-IOIFrames<NColCLB*CLBFrames)	// CLBs
//		{
//			*MJA=3+(FrameADR-CLKFrames-IOBFrames-IOIFrames)/CLBFrames;		// first CLB index is 3
//			*MNA=(FrameADR-CLKFrames-IOBFrames-IOIFrames)%CLBFrames;
//		}
//		else if(FrameADR-CLKFrames-IOBFrames-IOIFrames-NColCLB*CLBFrames<IOIFrames)
//		{																// right IOI
//			*MJA=NColCLB+3;
//			*MNA=FrameADR-CLKFrames-IOBFrames-IOIFrames-NColCLB*CLBFrames;
//		}
//		else															// right IOB
//		{	
//			*MJA=NColCLB+4;
//			*MNA=FrameADR-CLKFrames-IOBFrames-IOIFrames-NColCLB*CLBFrames-IOIFrames;
//		}
//	}
//	else if(FrameADR<CLKFrames+2*IOBFrames+2*IOIFrames+NColCLB*CLBFrames+NColRAM*RAMFrames)
//																		// we are in the RAM section
//	{
//		*BA=1;
//		*MJA=(FrameADR-CLKFrames-2*IOBFrames-2*IOIFrames-NColCLB*CLBFrames)/RAMFrames;
//		*MNA=(FrameADR-CLKFrames-2*IOBFrames-2*IOIFrames-NColCLB*CLBFrames)%RAMFrames;
//	}
//	else																// we are in the RAM interconnect section
//	{
//		*BA=2;
//		*MJA=(FrameADR-CLKFrames-2*IOBFrames-2*IOIFrames-NColCLB*CLBFrames-NColRAM*RAMFrames)/RI_Frames;
//		*MNA=(FrameADR-CLKFrames-2*IOBFrames-2*IOIFrames-NColCLB*CLBFrames-NColRAM*RAMFrames)%RI_Frames;
//	}
//	return(0);
//}
