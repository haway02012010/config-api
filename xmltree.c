/**
 * section: Tree
 * synopsis: Navigates a tree to print element names
 * purpose: Parse a file to a tree, use xmlDocGetRootElement() to
 *          get the root element, then walk the document and print
 *          all the element name in document order.
 * usage: tree1 filename_or_URL
 * test: tree1 test2.xml > tree1.tmp && diff tree1.tmp $(srcdir)/tree1.res
 * author: Dodji Seketeli
 * copy: see Copyright for the status of this software.
 */
#include "xmltree.h"

#ifdef LIBXML_TREE_ENABLED

/*
 *To compile this file using gcc you can type
 *gcc `xml2-config --cflags --libs` -o xmlexample libxml2-example.c
 */

/**
 * print_element_names:
 * @a_node: the initial xml node to consider.
 *
 * Prints the names of the all the xml elements
 * that are siblings or children of a given xml node.
 */
void
print_element_names(xmlNode * a_node)
{
    xmlNode *cur_node = NULL;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE)
            printf("node type: Element, name: %s\n", cur_node->name);

        print_element_names(cur_node->children);
    }
}

void
parseContent(xmlDocPtr doc, xmlNode * a_node)
{
    xmlNode *cur_node = NULL;
    xmlChar *key;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            printf("node type: Element, name: %s\n", cur_node->name);
            if ((!xmlStrcmp(cur_node->name, (const xmlChar *)"hw")) 
               || (!xmlStrcmp(cur_node->name, (const xmlChar *)"target"))
               || (!xmlStrcmp(cur_node->name, (const xmlChar *)"resource-string"))) {
                key = xmlNodeListGetString(doc, cur_node->children, 1);
                printf("keyword: %s\n", key);
                xmlFree(key);
            }
        }

        parseContent(doc, cur_node->children);
    }
}

void
parseRS(xmlDocPtr doc, xmlNode * a_node)
{
    xmlNode *cur_node = NULL;
    xmlChar *key;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            if (!xmlStrcmp(cur_node->name, (const xmlChar *)"resource-string")) {
                key = xmlNodeListGetString(doc, cur_node->children, 1);
                if (RS == NULL)
                    RS = malloc(strlen(key)*sizeof(char));
                strcpy(RS, key);
                xmlFree(key);
            }
        }

        parseRS(doc, cur_node->children);
    }
}

void
parseTarget(xmlDocPtr doc, xmlNode * a_node)
{
    xmlNode *cur_node = NULL;
    xmlChar *key;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            if (!xmlStrcmp(cur_node->name, (const xmlChar *)"target")) {
                key = xmlNodeListGetString(doc, cur_node->children, 1);
                if (target == NULL)
                    target = malloc(strlen(key)*sizeof(char));
                strcpy(target, key);
                xmlFree(key);
            }
        }

        parseTarget(doc, cur_node->children);
    }
}

void
parseLength(xmlDocPtr doc, xmlNode * a_node)
{
    xmlNode *cur_node = NULL;
    xmlChar *key;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            if (!xmlStrcmp(cur_node->name, (const xmlChar *)"length")) {
                key = xmlNodeListGetString(doc, cur_node->children, 1);
                bitstream_len = atoi(key);
                xmlFree(key);
            }
        }

        parseLength(doc, cur_node->children);
    }
}

void
parseIsland(xmlDocPtr doc, xmlNode * a_node)
{
    xmlNode *cur_node = NULL;
    xmlChar *key;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            if (!xmlStrcmp(cur_node->name, (const xmlChar *)"island")) {
                key = xmlNodeListGetString(doc, cur_node->children, 1);
                island = atoi(key);
                xmlFree(key);
            }
        }

        parseIsland(doc, cur_node->children);
    }
}

void
parseBitstream(xmlDocPtr doc, xmlNode * a_node)
{
    xmlNode *cur_node = NULL;
    xmlChar *key;

    char subkey[9];
    int getkey;
    int i, j, len;
    subkey[8] = NULL;
    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            if (!xmlStrcmp(cur_node->name, (const xmlChar *)"bitstream")) {
                key = xmlNodeListGetString(doc, cur_node->children, 1);
                len = strlen(key);
                if (inData == NULL)
                    inData = malloc(bitstream_len*sizeof(unsigned int));
                j = 0;

                for (i = 0; i < len; i= i+8) {
                    subkey[0] = key[i];
                    subkey[1] = key[i+1];
                    subkey[2] = key[i+2];
                    subkey[3] = key[i+3];
                    subkey[4] = key[i+4];
                    subkey[5] = key[i+5];
                    subkey[6] = key[i+6];
                    subkey[7] = key[i+7];

                    getkey = (int)strtol(subkey, NULL, 16);
                    inData[j++] = getkey;
                }
                xmlFree(key);
            }
        }

        parseBitstream(doc, cur_node->children);
    }
}
/**
 * Simple example to parse a file called "file.xml", 
 * walk down the DOM, and print the name of the 
 * xml elements nodes.
 */
#if 0
int
main(int argc, char **argv)
{
    xmlDoc *doc = NULL;
    xmlNode *root_element = NULL;

    int i;
    if (argc != 2)
        return(1);

    /*
     * this initialize the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used.
     */
    LIBXML_TEST_VERSION

    /*parse the file and get the DOM */
    doc = xmlReadFile(argv[1], NULL, 0);

    if (doc == NULL) {
        printf("error: could not parse file %s\n", argv[1]);
    }

    /*Get the root element node */
    root_element = xmlDocGetRootElement(doc);

    print_element_names(root_element);

    parseTarget(doc, root_element);
    printf("target device %s\n", target);
    parseRS(doc, root_element);
    printf("resource string %s\n", RS);
    parseLength(doc, root_element);
    printf("bitstream length %d\n", bitstream_len);
    parseIsland(doc, root_element);
    printf("device island %d\n", island);
    parseBitstream(doc, root_element);
    printf("inData array\n");
//    for (i = 0; i < bitstream_len; i++)
//        printf("0x%08x\n", inData[i]);

    if (RS != NULL)
        free(RS);
    if (target != NULL)
        free(target);
    if (inData != NULL)
        free(inData);
    /*free the document */
    xmlFreeDoc(doc);

    /*
     *Free the global variables that may
     *have been allocated by the parser.
     */
    xmlCleanupParser();

    return 0;
}
#endif
#else
int main(void) {
    fprintf(stderr, "Tree support not compiled in\n");
    exit(1);
}
#endif
