#include "config-api.h"

// How the island represents for clock region on the XC7Z020 device
// Clock region X0Y2: island 0. Clock region X1Y2: island 1
// Clock region X0Y1: island 2. Clock region X1Y2: island 3
// Clock region X0Y0: island 4. Clock region X1Y2: island 5

// Region is available when the resource[i][j] is 0 while it is occupied when 1
int **resource;

struct island *islands;

int init_resource(char *Device)
{
    int x, y;
    int i, j;
    int NoOfIslands;

    if (strcmp(Device,"XC7Z020") != 0) {
        printf("Device is not supporting!\n");
        return -1;
    }

    NoOfIslands = 6;
    islands = malloc(NoOfIslands * sizeof(island));
    if (islands == NULL) {
        printf("Failed to allocate the islands pointer\n");
        return -1;
    }
    for (i = 0; i < NoOfIslands; i++)
        islands[i].avail = 0;

    return 0;
}

int check_available_region(int island)
{
    int x, y;

    if (strcmp(Device,"XC7Z020") != 0) {
        printf("Device is not supporting!\n");
        return -1;
    }

    return islands[island].avail;
}

int set_occupied_region(int island)
{
    int x, y;

    if (strcmp(Device,"XC7Z020") != 0) {
        printf("Device is not supporting!\n");
        return -1;
    }

    islands[island].avail = 1;
    return 0;
}

int reset_island(int island)
{
    int x, y;

    if (strcmp(Device,"XC7Z020") != 0) {
        printf("Device is not supporting!\n");
        return -1;
    }

    islands[island].avail = 0;
    return 0;
}

int report_available_regions()
{
    int i;
    int NoOfIslands;
    if (strcmp(Device,"XC7Z020") != 0) {
        printf("Device is not supporting!\n");
        return -1;
    }
    NoOfIslands = 6;
    printf("Available islands are ");
    for (i = 0; i < NoOfIslands; i++)
        if (!check_available_region(i))
            printf(" %d", i);
    printf("\n");
    return 0;
}

int report_running_regions()
{
    int i;
    int NoOfIslands;
    if (strcmp(Device,"XC7Z020") != 0) {
        printf("Device is not supporting!\n");
        return -1;
    }
    NoOfIslands = 6;
    printf("Occupied islands are ");
    for (i = 0; i < NoOfIslands; i++)
        if (check_available_region(i) == 1)
            printf(" %d", i);
    printf("\n");
    return 0;
}

int place_accelerator_ptr(FILE *BitFile, int island)
{
    // Parse data from the XML metafile
    xmlDoc *doc = NULL;
    xmlNode *root_element = NULL;

    int i, tmp;
    int returnOfCheckRM;
    int inSize;
    int PartialCfg = 1;

    u32 PartialAddress;
    int Status;
    u32 IntrStsReg = 0;
    u32 StatusReg;

    int res_avail;
    // check if the resource island is available
    res_avail = check_available_region(island);
    if (res_avail != 0) {
        printf("Resource island is not available\n");
        return -1;
    }
    // set that island occupied
    set_occupied_region(island);
    /*parse the file and get the DOM */
    doc = xmlReadFile(BitFile, NULL, 0);

    if (doc == NULL) {
        printf("error: could not parse file %s\n", BitFile);
        return -1;
    }

    /*Get the root element node */
    root_element = xmlDocGetRootElement(doc);

//    print_element_names(root_element);

    parseTarget(doc, root_element);
    parseRS(doc, root_element);
    parseLength(doc, root_element);
    parseIsland(doc, root_element);
    parseBitstream(doc, root_element);

    inSize = sizeof(inData)/sizeof(unsigned int);
    // Bitstream relocation
    strcpy(Device, target);
    SetGlobalDeviceParameters(Device);

    // Check resource footprint matching
    returnOfCheckRM = checkResourceMatching(RS, island);
    if (returnOfCheckRM) {
        printf("Resource footprint mismatched!\n");
        return -1;
    }

    // Change FAR value
    for (i = 0; i < inSize; i++) {
        if (inData[i] == 0x30002001) {
            printf("old FAR value 0x%08x\n", inData[i+1]);
            tmp = calNewFAR(island);
            if (tmp < 0)
                printf("Failed to cal new FAR value\n");
            else {
                printf("new FAR value 0x%08x\n", tmp);
                inData[i+1] = tmp;
            }
        }
    }

    // Swap byte to send to PCAP port
    outData = (unsigned int *)malloc(inSize * sizeof(int));
    ByteSwap(inData, outData, inSize);
    // Send data to PCAP driver
    // Flush and disable Data Cache
    Xil_DCacheDisable();
    // Invalidate and enable Data Cache
    Xil_DCacheEnable();
    
    // Initialize Device Configuration Interface
    DcfgInstPtr = &DcfgInstance;
    XDcfg_0 = XDcfg_LookupConfig(XPAR_XDCFG_0_DEVICE_ID) ;
    Status =  XDcfg_CfgInitialize(DcfgInstPtr, XDcfg_0, XDcfg_0->BaseAddr);
    if (Status != XST_SUCCESS) {
    	return XST_FAILURE;
    }
    // Enable the pcap clock.
    StatusReg = Xil_In32(SLCR_PCAP_CLK_CTRL);
    if (!(StatusReg & SLCR_PCAP_CLK_CTRL_EN_MASK)) {
    	Xil_Out32(SLCR_UNLOCK, SLCR_UNLOCK_VAL);
    	Xil_Out32(SLCR_PCAP_CLK_CTRL,
    			(StatusReg | SLCR_PCAP_CLK_CTRL_EN_MASK));
    	Xil_Out32(SLCR_UNLOCK, SLCR_LOCK_VAL);
    }

    // Disable the level-shifters from PS to PL.
    if (!PartialCfg) {
    	Xil_Out32(SLCR_UNLOCK, SLCR_UNLOCK_VAL);
    	Xil_Out32(SLCR_LVL_SHFTR_EN, 0xA);
    	Xil_Out32(SLCR_LOCK, SLCR_LOCK_VAL);
    }

    // Select PCAP interface for partial reconfiguration
    if (PartialCfg) {
    	XDcfg_EnablePCAP(DcfgInstPtr);
    	XDcfg_SetControlRegister(DcfgInstPtr, XDCFG_CTRL_PCAP_PR_MASK);
    }

    // Clear the interrupt status bits
    XDcfg_IntrClear(DcfgInstPtr, (XDCFG_IXR_PCFG_DONE_MASK |
    				XDCFG_IXR_D_P_DONE_MASK |
    				XDCFG_IXR_DMA_DONE_MASK));
    
    // Check if DMA command queue is full
    StatusReg = XDcfg_ReadReg(DcfgInstPtr->Config.BaseAddr,
    			XDCFG_STATUS_OFFSET);
    if ((StatusReg & XDCFG_STATUS_DMA_CMD_Q_F_MASK) ==
    		XDCFG_STATUS_DMA_CMD_Q_F_MASK) {
    	return XST_FAILURE;
    }

    PartialAddress = (u32)outData;
    Status = XDcfg_TransferBitfile(DcfgInstPtr, PartialCfg, PartialAddress, inSize);
    if (Status != XST_SUCCESS) {
    	xil_printf("Error : FPGA configuration failed!\n\r");
    	exit(EXIT_FAILURE);
    }

    // Free malloced variables
    if (RS != NULL)
        free(RS);
    if (target != NULL)
        free(target);
    if (inData != NULL)
        free(inData);
    if (outData != NULL)
        free(outData);
    /*free the document */
    xmlFreeDoc(doc);

    /*
     *Free the global variables that may
     *have been allocated by the parser.
     */
    xmlCleanupParser();
    return 0;
}
