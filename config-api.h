#include "Devices.h"
#include "bitman.h"
#include "xmltree.h"
#include "pcap.h"

int init_resource(char *Device);
int check_available_region(int island);
int set_occupied_region(int island);
int reset_island(int island);
int report_available_regions();
int report_running_regions();
int place_accelerator_ptr(FILE *BitFile, int island);

struct island
{
    int from_X;
    int from_Y;
    int to_X;
    int to_Y;
    int avail;
};
