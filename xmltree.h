#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

char *target;
char *RS;
int bitstream_len;
int island;
extern unsigned int *inData;

void print_element_names(xmlNode * a_node);
void parseContent(xmlDocPtr doc, xmlNode * a_node);
void parseRS(xmlDocPtr doc, xmlNode * a_node);
void parseTarget(xmlDocPtr doc, xmlNode * a_node);
void parseLength(xmlDocPtr doc, xmlNode * a_node);
void parseBitstream(xmlDocPtr doc, xmlNode * a_node);
