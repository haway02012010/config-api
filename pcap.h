#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "xparameters.h"
//#include "xil_printf.h"
//#include "xil_cache.h"
//#include "ff.h"
#include "xdevcfg.h"
#include "xil_io.h"
#include "xil_types.h"

// SLCR registers
#define SLCR_LOCK       0xF8000004 // SLCR Write Protection Lock
#define SLCR_UNLOCK     0xF8000008 // SLCR Write Protection Unlock
#define SLCR_LVL_SHFTR_EN 0xF8000900 // SLCR Level Shifters Enable
#define SLCR_PCAP_CLK_CTRL XPAR_PS7_SLCR_0_S_AXI_BASEADDR + 0x168 // SLCR PCAP clock control register address

#define SLCR_PCAP_CLK_CTRL_EN_MASK 0x1
#define SLCR_LOCK_VAL   0x767B
#define SLCR_UNLOCK_VAL 0xDF0D
// Turn on/off Debug messages
#ifdef DEBUG_PRINT
#define  debug_printf  xil_printf
#else
#define  debug_printf(msg, args...) do {  } while (0)
#endif

// Driver Instantiations
static XDcfg_Config *XDcfg_0;
XDcfg DcfgInstance;
XDcfg *DcfgInstPtr;
